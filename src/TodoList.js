import React, { Component } from "react";
import TodoItem from "./TodoItem";
import { connect } from "react-redux";
import { toggleTodo, deleteTodo } from "./actions";

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map(todo => (
            <TodoItem
              key={todo.id}
              handleDelete={event => this.props.deleteTodo(todo.id)}
              handleToggle={event => this.props.toggleTodo(todo.id)}
              title={todo.title}
              completed={todo.completed}
            />
          ))}
        </ul>
      </section>
    );
  }
}

const mapDispatchToProps = {
  toggleTodo,
  deleteTodo
};

export default connect(null, mapDispatchToProps)(TodoList);
