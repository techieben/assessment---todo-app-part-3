import React, { Component } from "react";
import "./index.css";
import todosList from "./todos.json";
import { Route, NavLink } from "react-router-dom";
import TodoList from "./TodoList";
import { connect } from "react-redux";
import { addTodo, clearCompletedTodos } from "./actions";

class App extends Component {
  state = {
    todos: todosList,
    value: ""
  };

  handleChange = event => {
    this.setState({ value: event.target.value });
  };

  handleCreate = event => {
    if (event.key === "Enter") {
      this.props.addTodo(this.state.value);
      this.setState({ value: "" });
    }
  };

  handleDelete = (event, todoIdToDelete) => {
    const newTodoList = this.state.todos.filter(
      todo => todo.id !== todoIdToDelete
    );
    this.setState({ todos: newTodoList });
  };

  // handleClear = event => {
  //   const newTodoList = this.state.todos.filter(
  //     todo => todo.completed === false
  //   );
  //   this.setState({ todos: newTodoList });
  // };

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autoFocus
            onKeyDown={this.handleCreate}
            onChange={this.handleChange}
            value={this.state.value}
          />
        </header>
        <Route
          exact
          path="/"
          render={() => (
            <TodoList
              handleDelete={this.handleDelete}
              handleToggle={this.handleToggle}
              todos={this.props.todos}
            />
          )}
        />
        <Route
          path="/active"
          render={() => (
            <TodoList
              handleDelete={this.handleDelete}
              handleToggle={this.handleToggle}
              todos={this.props.todos.filter(todo => todo.completed === false)}
            />
          )}
        />
        <Route
          path="/completed"
          render={() => (
            <TodoList
              handleDelete={this.handleDelete}
              handleToggle={this.handleToggle}
              todos={this.props.todos.filter(todo => todo.completed === true)}
            />
          )}
        />
        <footer className="footer">
          {/* <!-- This should be `0 items left` by default --> */}
          <span className="todo-count">
            <strong>
              {this.props.todos.filter(todo => todo.completed === false).length}
            </strong>{" "}
            item(s) left
          </span>
          <ul className="filters">
            <li>
              <NavLink to="/" activeClassName="selected" exact>
                All
              </NavLink>
            </li>
            <li>
              <NavLink to="/active" activeClassName="selected">
                Active
              </NavLink>
            </li>
            <li>
              <NavLink to="/completed" activeClassName="selected">
                Completed
              </NavLink>
            </li>
          </ul>
          <button
            className="clear-completed"
            onClick={this.props.clearCompletedTodos}
          >
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}

// mapStateToProps
const mapStateToProps = state => {
  return {
    todos: state.todos
  };
};

// mapDispatchToProps
const mapDispatchToProps = {
  addTodo,
  clearCompletedTodos
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
